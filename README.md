### esm-init

[Source Code on Gitlab](https://gitlab.com/nxtcoder17/npm-esm-init)

> How to Use ?
```bash
npx/pnpx esm-init
```
> or, install it globally using `npm/pnpm i -g esm-init`, and then

```bash
esm-init
```
