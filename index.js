#! /usr/bin/env node

import {exec} from 'child_process';
import fs from 'fs/promises';
import path from 'path';
import {fileURLToPath} from 'url';


await new Promise((resolve, reject) => {
  exec('pnpm init -y', (err, stdout, stderr) => {
    if (err) {
      reject(err);
    }
    resolve(true)
  })
});

const {default: pjson} = await import(`${process.cwd()}/package.json`);

await fs.writeFile('./package.json', JSON.stringify(
  Object.keys(pjson).reduce((acc, key) => {
    if (key === 'scripts') {
      acc['type'] = 'module';
    }
    acc[key] = pjson[key];
    return acc;
  }, {}), null, 2));
