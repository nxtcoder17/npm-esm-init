#! /usr/bin/env sh

export NODE_NO_WARNINGS=1
d=$(realpath $(dirname "$0"))
node --es-module-specifier-resolution=node $d/index.js
